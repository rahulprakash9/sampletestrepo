package com.restapi.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

	private static Map<String, User> UserRepo = new HashMap<>();
	static {
		User user = new User();
		user.setId("101");
		user.setName("Rahul");
		user.setCity("Bangalore");
		UserRepo.put(user.getId(), user);

	}

	@RequestMapping(value = "/Users", method = RequestMethod.GET)
	public ResponseEntity<Object> getProducts(@RequestBody User user) {

		user.getId();
		user.setCity(user.getCity());
		user.setName(user.getName());

		return new ResponseEntity<>(UserRepo.values(), HttpStatus.OK);

	}

	public User getProductValueBasedOnId(User user) {
		return user;

	}

	@RequestMapping(value = "/Users", method = RequestMethod.POST, consumes = { "application/json" })
	public ResponseEntity<Object> createProduct(@RequestBody User users) {
		UserRepo.put(users.getId(), users);
		System.out.println("Users" + users.getId());

		return new ResponseEntity<>("User is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/Users/{id}", method = RequestMethod.PUT, consumes = { "application/json" })
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody User user) {

		UserRepo.remove(id);
		user.setId(id);
		UserRepo.put(id, user);

		return new ResponseEntity<>("User is updated successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/Users/{id}", method = RequestMethod.DELETE, consumes = { "application/json" })
	public ResponseEntity<Object> delete(@PathVariable("id") String id) {
		System.out.println("User details before removing " + id);
		UserRepo.remove(id);
		System.out.println("Userdetails after removing " + id);
		return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
	}

}
